# Cyberex data converter
*Author: Ondřej Budai <budai@mail.muni.cz>*

## How to use
Create directory somewhere on your computer. Put there folder with e-mails, check definitions and database dump in json format. The structure shall look as following:

```
dir/emails
   /check.yml
   /database.json
```

Install pipenv environment:
```
pipenv install
```

Run the converter:
```
pipenv run python data_converter.py DIR >new-database.json
```

File `new-database.json` shall contain all the data from original dump with e-mails and check definitions.
