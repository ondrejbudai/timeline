import json
import re
import sys

from checks_converter import insert_important_into_database
from mail_converter import get_emails


def get_teams_from_database(database):
    return filter(lambda team: team["model"] == "cyberex_api.teams", database)


def get_kypo_id(team):
    raw_kypo_id = team["fields"]["kypoID"]
    search_obj = re.search('([0-9]+)$', raw_kypo_id)
    return int(search_obj.group(1))


def create_kypo_id_to_team_id_mapping(database):
    teams = get_teams_from_database(database)

    return {get_kypo_id(team): team["pk"] for team in teams}


SRC_DIR = '../data/raw'

if len(sys.argv) == 2:
    SRC_DIR = sys.argv[1]
elif len(sys.argv) > 2:
    print('ERROR: Provide zero or one argument with path to raw data!', file=sys.stderr)
    exit(1)

database = json.load(open(SRC_DIR + '/database.json'))

database = list(insert_important_into_database(database, SRC_DIR))
database.extend(get_emails(SRC_DIR, create_kypo_id_to_team_id_mapping(database)))

print(json.dumps(database))
