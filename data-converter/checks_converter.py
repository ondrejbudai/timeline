import yaml


def create_check_key(check):
    return f"{check['checkId']}{check['srcHostname']}{check['dstHostname']}"


def get_checks(data_dir):
    file = open(data_dir + '/checks.yml', 'r')

    data = yaml.load(file, yaml.Loader)

    return {create_check_key(check["fields"]): check["fields"] for check in data}


def insert_important_into_record(record, checks):
    if record["model"] != "cyberex_api.automaticlogs":
        return record
    record["fields"]["critical"] = checks[create_check_key(record["fields"])]["important"]
    return record


def insert_important_into_database(data, data_dir):
    checks = get_checks(data_dir)
    return map(lambda record: insert_important_into_record(record, checks), data)
