import email
from datetime import datetime
from email import policy
from itertools import chain
from mailbox import Maildir


def convert_mail_date_field_to_iso_datetime(timestamp):
    # [:31] - we need to truncate (CEST) on end of some messages
    date = datetime.strptime(timestamp[:31], '%a, %d %b %Y %H:%M:%S %z')
    return date.isoformat()


# def get_attachments(message):
#     attachments = list(message.iter_attachments())
#     for attachment in attachments:
#         print(attachment.get('Content-Disposition'))
#         print(attachment.get_payload(decode=True))
#         print('**************')
#     return attachments


def convert_message(message_tuple, directory, team_id):
    message_id, old_message = message_tuple

    # newer EmailMessage class has better interface than that returned from Maildir().items()
    message = email.message_from_bytes(old_message.as_bytes(), policy=policy.default)
    return {
        'model': 'cyberex_api.emails',
        'pk': None,
        'fields': {
            'messageId': message.get('Message-ID'),
            'timestamp': convert_mail_date_field_to_iso_datetime(message.get('Date')),
            'addressFrom': message.get('From'),
            'addressTo': message.get('To'),
            'inReplyTo': message.get('In-Reply-To'),
            'subject': message.get('Subject'),
            'body': message.get_body().get_content(),
            'directory': directory,
            'teamId': team_id,
            # 'attachments': get_attachments(message)
        }
    }


def get_messages_from_station(base_directory, kypo_id, team_id):
    return chain(
        map(lambda m: convert_message(m, 'received', team_id),
            Maildir(f"{base_directory}/cyberczech.ex/station{kypo_id}").items()),
        map(lambda m: convert_message(m, 'sent', team_id),
            Maildir(f"{base_directory}/cyberczech.ex/station{kypo_id}/.Sent").items()),
    )


def get_emails(dir_path, kypo_id_to_team_id_mapping):
    mails = []

    for kypo_id in range(1, 7):
        if kypo_id not in kypo_id_to_team_id_mapping:
            continue
        team_id = kypo_id_to_team_id_mapping[kypo_id]
        mails.extend(get_messages_from_station(dir_path + '/emails', kypo_id, team_id))
    return mails
