import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ManualLogService } from '../../services/manual-log.service';
import { StoreService } from '../../services/store.service';
import { map } from 'rxjs/operators';
import { TimeConverterService } from '../../services/time-converter.service';
import { Observable } from 'rxjs';
import { TimelinePoint } from '../../util-interfaces/timeline-point';
import { ManualLogEntry } from '../../entities/manual-log-entry';

type LogType = Observable<{ role: string, entries: TimelinePoint<ManualLogEntry>[] }[]>;

/** Column with manual log */
@Component({
  selector: 'app-manual-log',
  templateUrl: './manual-log.component.html',
  styleUrls: ['./manual-log.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManualLogComponent {

  constructor(
    private readonly manualLogService: ManualLogService,
    private readonly settings: StoreService,
    private readonly timeConverter: TimeConverterService
  ) { }

  /** Logs grouped by role */
  readonly log$: LogType = this.manualLogService.manualLogGroupedByRole$.pipe(map((logGroups) => {
    // Convert logs to TimelinePoint as this is the format which is used everywhere to render timeline points
    return logGroups.map(({role, entries}) => {
      return {
        role,
        entries: entries.map(entry => this.timeConverter.toTimelinePointFromISODateTime(entry, entry.timestamp))
      };
    });
  }));

}
