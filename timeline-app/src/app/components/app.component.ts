import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

/** Main application component */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'timeline';


  constructor(router: Router) {
    router.events.subscribe((event) => {
      // MouseFlow integration
      if (!(event instanceof NavigationEnd)) {
        return;
      }

      (window as any)._mfq.push(['newPageView', event.url]);
    });
  }
}
