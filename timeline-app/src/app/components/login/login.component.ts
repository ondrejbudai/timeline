import { Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  loading = false;

  constructor(
    private oAuthService: OAuthService,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }


  ngOnInit() {
  }

  async login() {
    try {
      this.loading = true;
      await this.oAuthService.fetchTokenUsingPasswordFlow(this.username, this.password);
      this.router.navigate(['/timeline']);
    } catch (e) {
      this.snackBar.open('Bad login password combination!', undefined, {duration: 2000});
    }

    this.loading = false;

  }
}
