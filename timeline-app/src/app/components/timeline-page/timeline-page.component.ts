import { Component, OnInit } from '@angular/core';
import { Team } from '../../entities/team';
import { StoreService } from '../../services/store.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { LoaderService } from '../../services/loader.service';
import { zip } from 'rxjs';

/** Container for timeline and detail panel. */
@Component({
  selector: 'app-timeline-page',
  templateUrl: './timeline-page.component.html',
  styleUrls: ['./timeline-page.component.scss']
})
export class TimelinePageComponent implements OnInit {

  /** Array of blue teams participating in exercise. */
  teams = new Array<Team>();

  /** Flag determining if teams are loaded (aka is application initialized?) */
  initialized = false;

  /** Flag determining if data for one team are currently being loaded */
  loading = false;

  constructor(
    private readonly apiService: ApiService,
    public readonly store: StoreService,
    private readonly oAuthService: OAuthService,
    private readonly router: Router,
    private readonly loaderService: LoaderService
  ) { }

  /**
   * Load teams and exercise and opens the data for first team
   */
  ngOnInit() {
    zip(this.apiService.getTeams(), this.loaderService.loadExercise()).subscribe(([teams]) => {
      this.teams = teams;
      this.selectTeam(teams[0]);
      this.initialized = true;
    });
  }

  /**
   * Loads team data and shows it.
   * @param team Team to load and show
   */
  selectTeam(team: Team) {
    this.store.team = team;
    this.loading = true;
    this.loaderService.load(team).subscribe(() => {
      this.loading = false;

      // data for mouseflow
      (window as any)._mfq.push(['newPageView', '/timeline/loaded']);
    });
  }

  /**
   * Logs out the user from application.
   */
  logout() {
    this.oAuthService.logOut();
    this.router.navigate(['/']);

    return false;
  }
}
