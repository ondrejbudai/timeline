import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AutomaticLogService } from '../../services/automatic-log.service';
import { Observable } from 'rxjs';
import { AutomaticLogEntry, Check } from '../../entities/automatic-log-entry';
import { TimelineInterval } from '../../util-interfaces/timeline-point';

type LogType = Observable<{
  name: string,
  totalColumns: number,
  columns: { destination: string, columns: { check: Check, intervals: TimelineInterval<AutomaticLogEntry>[] }[] }[]
}[]>;

/** Automatic log column. */
@Component({
  selector: 'app-automatic-log',
  templateUrl: './automatic-log.component.html',
  styleUrls: ['./automatic-log.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomaticLogComponent {
  constructor(
    private readonly automaticLogService: AutomaticLogService
  ) { }

  readonly logGroups$: LogType = this.automaticLogService.logGroups$;
}
