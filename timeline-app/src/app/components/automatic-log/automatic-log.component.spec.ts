import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomaticLogComponent } from './automatic-log.component';

describe('AutomaticLogComponent', () => {
  let component: AutomaticLogComponent;
  let fixture: ComponentFixture<AutomaticLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutomaticLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomaticLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
