import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { TimelineInterval, TimelinePoint } from '../../util-interfaces/timeline-point';
import { Email } from '../../entities/email';
import { IntervalSchedulerService } from '../../services/interval-scheduler.service';
import { flatMap, maxBy, minBy } from 'lodash-es';
import { StoreService } from '../../services/store.service';
import { TimeConverterService } from '../../services/time-converter.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

type EmailWithColorClass = Email & { colorClass: string };
type ColsType =
  Observable<{
    threads: TimelineInterval<TimelinePoint<Email>[]>[],
    emails: TimelinePoint<EmailWithColorClass>[]
  }[]>;

/** Email column */
@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmailComponent {
  constructor(
    private readonly apiService: ApiService,
    private readonly intervalSchedulerService: IntervalSchedulerService,
    private readonly store: StoreService,
    private readonly timeConverterService: TimeConverterService
  ) { }

  /* E-mail threads grouped to columns using scheduling algorithm. */
  emailThreadsCols$: ColsType = this.store.emails$.pipe(map((emails) => {
    const emailPoints = emails.map((email) => {
      // Remove previous messages from body and add color coding
      return {
        ...email,
        body: email.body.replace(/(((On)|(Dne)).*\n)?^>.*\n?/gm, ''),
        colorClass: getColorClassForEmail(extractNonBlueTeamParticipant(email))
      };
    }).map(email => this.timeConverterService.toTimelinePointFromISODateTime(email, email.timestamp));

    const emailThreads = this.groupEmailsToThreads(emailPoints);

    // Use scheduling algorithm to group threads into subcolumns with no overlapping threads
    return this.intervalSchedulerService.schedule(emailThreads, 10 / 100).map((colThreads) => {
      return {
        threads: colThreads,
        emails: flatMap(colThreads, colThread => colThread.data),
      };
    });
  }));

  /**
   * Groups e-mails to threads
   * @param emails E-mails to group
   */
  private groupEmailsToThreads(emails: TimelinePoint<EmailWithColorClass>[]) {
    // Sort e-mails ascended
    const emailsAscended = emails.sort((a, b) => a.y - b.y);
    const groups: TimelinePoint<EmailWithColorClass>[][] = [];
    const messageIdToGroup = new Map<string, TimelinePoint<EmailWithColorClass>[]>();

    // If e-mail has parent in some group, add it into that group
    // If e-mail hasn't parent in any group, create new group
    for (const email of emailsAscended) {
      let group;

      if (!email.data.inReplyTo) {
        group = [email];
        groups.push(group);
      } else {
        group = messageIdToGroup.get(email.data.inReplyTo);
        if (!group) {
          console.warn(`cannot find parent ${email.data.inReplyTo} for email ${email.data.messageId}!`);
          group = [email];
          groups.push(group);
        } else {
          group.push(email);
        }
      }

      messageIdToGroup.set(email.data.messageId, group);
    }

    // Add duration to each group
    return groups.map((emailGroup) => {
      const start = minBy(emailGroup, email => email.y).y;
      const duration = maxBy(emailGroup, email => email.y).y - start;
      return {
        start,
        duration,
        data: emailGroup,
      };
    });
  }
}

/**
 * Extracts e-mail participant which is not from blue team.
 * @param email E-mail to extract participant from
 */
function extractNonBlueTeamParticipant(email: Email) {
  if (email.directory === 'received') {
    return email.addressFrom;
  }

  return email.addressTo;
}

/**
 * Creates color coding for e-mail address
 * @param emailAddress E-mail address
 */
function getColorClassForEmail(emailAddress: string) {
  if (emailAddress.startsWith('blondie')) {
    return 'blondie';
  } else if (emailAddress.endsWith('police.ex')) {
    return 'police';
  } else if (emailAddress.startsWith('incident')) {
    return 'incident';
  }

  return 'default';
}
