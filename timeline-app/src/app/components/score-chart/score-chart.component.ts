import { Component } from '@angular/core';
import { StoreService } from '../../services/store.service';
import { TimelinePoint } from '../../util-interfaces/timeline-point';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AutomaticLogEntry } from '../../entities/automatic-log-entry';
import { ManualLogEntry } from '../../entities/manual-log-entry';
import { START_SCORE } from '../../../globals';
import { TimeConverterService } from '../../services/time-converter.service';
import {
  TaggedAutomaticLogEntryWithScore,
  TaggedManualLogEntryWithScore
} from '../../util-interfaces/types';
import { AutomaticLogService } from '../../services/automatic-log.service';
import { ManualLogService } from '../../services/manual-log.service';

type LogEntry = AutomaticLogEntry | ManualLogEntry;
type DataType = Observable<{
  chartData: {d: string, stroke: string}[];
  scoreChanges: TimelinePoint<TaggedManualLogEntryWithScore | TaggedAutomaticLogEntryWithScore>[];
  minScore: number;
  maxScore: number;
  labels: string[];
}>;

/** Chart visualizing score progression during exercise */
@Component({
  selector: 'app-score-chart',
  templateUrl: './score-chart.component.html',
  styleUrls: ['./score-chart.component.scss']
})
export class ScoreChartComponent {

  constructor(
    private readonly store: StoreService,
    private readonly automaticLogService: AutomaticLogService,
    private readonly timeConverter: TimeConverterService,
    private readonly manualLogService: ManualLogService
  ) { }

  /** Chart data */
  data: DataType = combineLatest(this.store.automaticLog$, this.manualLogService.manualLogWithFullObjectives$).pipe(
    // combine automatic and manual log, filter out entries without score change and sort them
    map(([automaticLog, manualLog]) => {
      const combinedLog = [
        ...automaticLog
          .filter(logEntry => logEntry.points !== 0)
          .map(logEntry => ({ ...logEntry, tag: 'automatic-log-with-score' as 'automatic-log-with-score' })),
        ...manualLog
          .filter(logEntry => logEntry.points !== 0)
          .map(logEntry => ({ ...logEntry, tag: 'manual-log-with-score' as 'manual-log-with-score' })),
      ].map(logEntry => this.timeConverter.toTimelinePointFromISODateTime(logEntry, logEntry.timestamp))
        .sort((a, b) => a.y - b.y);

      const { minScore, maxScore } = getMinMaxScore(combinedLog, START_SCORE);

      let currentScore = START_SCORE;

      // calculate running score for all changes
      const scoreChanges = combinedLog.map((point) => {
        currentScore += point.data.points;
        return {
          y: point.y,
          data: {
            ...point.data,
            score: currentScore,
          }
        };
      });

      return {
        scoreChanges,
        chartData: getChartData(scoreChanges, minScore, maxScore),
        minScore,
        maxScore,
        labels: getLabels(minScore, maxScore)
      };
    })
  );
}

/**
 * Gets score minimum and maximum from array of changes
 * @param scoreChanges Array of score changes
 * @param startScore Starting score
 */
function getMinMaxScore(scoreChanges: TimelinePoint<LogEntry>[], startScore: number) {
  let currentScore = startScore;
  let minScore = currentScore;
  let maxScore = currentScore;

  for (const scoreChange of scoreChanges) {
    currentScore += scoreChange.data.points;
    minScore = Math.min(currentScore, minScore);
    maxScore = Math.max(currentScore, maxScore);
  }

  return { minScore, maxScore };
}

/**
 * Creates data for svg chart
 * @param scores Array of scores
 * @param minScore Team's minimum score
 * @param maxScore Team's maximum score
 *
 * This method creates array of polylines representing score changes.
 * Each polyline has defined its coordinates and color (stroke in SVG).
 */
function getChartData(scores: TimelinePoint<{ score: number; points: number }>[], minScore: number, maxScore: number) {
  const chartData = [];

  for (let i = 1; i < scores.length; ++i) {
    const prevScore = scores[i - 1];
    const curScore = scores[i];
    const hue = ((prevScore.data.score - minScore) / (maxScore - minScore)) * 120;
    chartData.push({
      d: `M ${prevScore.data.score} ${prevScore.y} L ${prevScore.data.score} ${curScore.y} L ${curScore.data.score} ${curScore.y}`,
      stroke: `hsl(${hue}, 100%, 35%)`
    });
  }

  return chartData;
}

/**
 * Creates labels shown in the subheader of score column
 * @param minScore Team's minimum score
 * @param maxScore Team's maximum score
 */
function getLabels(minScore: number, maxScore: number) {
  const numberOfLabels = 4;
  return new Array<string>(numberOfLabels + 1).fill('').map((_, index) => {
    const score = minScore + index * ((maxScore - minScore) / numberOfLabels);
    const ROUND_FACTOR = 1000;
    const roundedScore = Math.round(score / ROUND_FACTOR);
    return roundedScore.toString() + 'k';
  });
}
