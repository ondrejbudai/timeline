import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { StoreService } from '../../services/store.service';

/** Main component of timeline, contains all entity columns */
@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {
  @ViewChild('body') bodyElementRef: ElementRef;
  @ViewChild('bodyContainer') bodyContainerElementRef: ElementRef;

  constructor(
    public store: StoreService
  ) { }

  /**
   * Controls the timeline's zoom level, called when WheelEvent occurs
   * @param event WheelEvent
   */
  onWheel(event: WheelEvent) {
    // calculate mouse position
    const mousePos = event.clientY - this.bodyElement.getBoundingClientRect().top + this.bodyElement.scrollTop - 40;

    // compute the "time position" of mouse
    const oldTime = mousePos / this.store.pixelsPerHour;

    const newPixelsPerHour = this.store.pixelsPerHour - 2 * event.deltaY;
    const oldScroll = this.bodyElement.scrollTop;

    const currentMinPixelsPerHour = this.getCurrentMinPixelsPerHour();
    if (newPixelsPerHour < currentMinPixelsPerHour) {
      // write into the store only if it's a must, it triggers quite costly rerender
      if (this.store.pixelsPerHour !== currentMinPixelsPerHour) {
        this.store.pixelsPerHour = currentMinPixelsPerHour;
      }
      return false;
    } else {
      this.store.pixelsPerHour = newPixelsPerHour;
    }

    // calculate new position and change the scroll
    const newPos = this.store.pixelsPerHour * oldTime;
    const delta = newPos - mousePos;
    this.bodyElement.scrollTop = oldScroll + delta;

    return false;
  }

  /**
   * Controls the timeline's scroll position
   * @param event MouseEvent
   */
  onMouseMove(event: MouseEvent) {
    // Limit the trigger to left button
    // tslint:disable-next-line:no-bitwise
    if (!(event.buttons & 1)) {
      return;
    }

    this.bodyElement.scrollTop -= event.movementY;

    event.preventDefault();
  }

  /**
   * Returns timeline body element
   */
  private get bodyElement() {
    return this.bodyElementRef.nativeElement as Element;
  }

  /**
   * Returns timeline body container element
   */
  private get bodyContainerElement() {
    return this.bodyContainerElementRef.nativeElement as HTMLElement;
  }

  /**
   * Initializes the timeline
   */
  ngOnInit(): void {
    // Default zoom level is the lowest one
    this.store.pixelsPerHour = this.getCurrentMinPixelsPerHour();

    // Update minimum zoom level when resize event occurs
    window.addEventListener('resize', () => {
      if (this.getCurrentMinPixelsPerHour() > this.store.pixelsPerHour) {
        this.store.pixelsPerHour = this.getCurrentMinPixelsPerHour();
      }
    });

    // Next lines actually changes the zoom level
    // The zoom level is actually the height of timeline body container
    // It is not done using Angular data binding, because in one tick we need to update the height of container,
    // let the browser recalculate it and then change the scroll.
    // This is not possible to do using data binding in one tick. ANd if it happens in more ticks, flickering will
    // happen.
    this.store.pixelsPerHour$.subscribe((pixelsPerHour) => {
      this.bodyContainerElement.style.height = pixelsPerHour * this.store.exercise.duration + 'px';
    });
  }

  /**
   * Gets minimal zoom level from body height.
   */
  private getCurrentMinPixelsPerHour() {
    return Math.floor((this.bodyElement.clientHeight - 40) / this.store.exercise.duration);
  }
}
