import { Component } from '@angular/core';
import { TimeAxisService } from '../../services/time-axis.service';

/** Grid shown in the background of timeline */
@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent {
  constructor(
    private readonly timeAxisService: TimeAxisService
  ) {}

  readonly points$ = this.timeAxisService.timeAxisPoints$;

}
