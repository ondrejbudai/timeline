import { Component } from '@angular/core';
import { TimeAxisService } from '../../services/time-axis.service';


/** Time axis on the leftmost side of timeline. */
@Component({
  selector: 'app-time-axis',
  templateUrl: './time-axis.component.html',
  styleUrls: ['./time-axis.component.scss']
})
export class TimeAxisComponent {
  constructor(private readonly timeAxisService: TimeAxisService) {}

  readonly points$ = this.timeAxisService.timeAxisPoints$;
}
