import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ObjectiveService } from '../../services/objective.service';
import { Observable } from 'rxjs';
import { TimelineInterval } from '../../util-interfaces/timeline-point';
import { ObjectiveWithLog } from '../../entities/objective';


type ObjectivesType = Observable<TimelineInterval<ObjectiveWithLog>[][]>;

/** Objectives column */
@Component({
  selector: 'app-objectives',
  templateUrl: './objectives.component.html',
  styleUrls: ['./objectives.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ObjectivesComponent {
  constructor(
    private readonly objectiveService: ObjectiveService,
  ) { }

  /** Objectives grouped into columns using scheduling algorithm */
  readonly objectives$: ObjectivesType = this.objectiveService.groupedObjectives$;
}
