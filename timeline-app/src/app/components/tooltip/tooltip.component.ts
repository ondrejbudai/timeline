import { ChangeDetectionStrategy, Component } from '@angular/core';
import { TaggedData } from '../../util-interfaces/types';

/** Component representing the tooltip */
@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TooltipComponent {
  data: TaggedData;
}
