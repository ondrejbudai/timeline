import { Component, Input } from '@angular/core';
import { TaggedData } from '../../util-interfaces/types';
import { TimeConverterService } from '../../services/time-converter.service';

/** Info panel containing details about any entity type */
@Component({
  selector: 'app-info-detail',
  templateUrl: './info-detail.component.html',
  styleUrls: ['./info-detail.component.scss']
})
export class InfoDetailComponent {
  title: string;
  points: number;
  gameTime: string;
  /** Table containing all the details */
  table: { key: string, value: string, classes: string[] }[];


  constructor(
    private readonly timeConverter: TimeConverterService
  ) {}

  /** Flag determining whether this component shall use bigger or smaller styles */
  @Input() small = false;

  /**
   * Updates the info detail.
   * @param data data to update info detail
   */
  @Input()
  set data(data: TaggedData) {
    if (data.tag === 'manual-log' || data.tag === 'manual-log-with-score') {
      this.title = data.objective.name;
      this.points = data.points;
      this.gameTime = this.timeConverter.toTimeGameFromISODateTime(data.timestamp);
      this.table = [
        entry('Role', data.objective.role),
        entry('Phase', data.objective.phase),
      ];

      // Not all objectives associated with manual log have defined max/min points
      if (data.objective.minPoints !== undefined) {
        this.table.push(entry('Min points', data.objective.minPoints));
      }

      if (data.objective.maxPoints !== undefined) {
        this.table.push(entry('Min points', data.objective.maxPoints));
      }

      this.table.push(entry('Comment', data.comment));

    } else if (data.tag === 'automatic-log' || data.tag === 'automatic-log-with-score') {
      this.title = data.check_id;
      this.points = data.points;
      this.gameTime = this.timeConverter.toTimeGameFromISODateTime(data.timestamp);
      this.table = [
        entry('Source', data.src_hostname),
        entry('Destination', data.dst_hostname),
        entry('Check ID', data.check_id),
        entry('Critical', data.critical)
      ];
    } else if (data.tag === 'objective') {
      this.title = data.name;
      this.points = data.log.points;
      this.gameTime = data.startingTime;
      this.table = [
        entry('Abbreviation', data.abbreviation),
        entry('Phase', data.phase),
        entry('Duration', data.duration),
        entry('Notify starting time', data.notifyStartingTime),
        entry('Min points', data.minPoints),
        entry('Max points', data.maxPoints),
        entry('Role', data.role),
      ];
    } else if (data.tag === 'email') {
      this.title = data.subject;
      this.points = undefined;
      this.gameTime = this.timeConverter.toTimeGameFromISODateTime(data.timestamp);
      this.table = [
        entry('From', data.addressFrom),
        entry('To', data.addressTo),
      ];

      // Do not put body in smaller InfoDetail variant
      if (!this.small) {
        this.table.push(entry('Body', data.body, ['pre', 'small']));
      }
    } else if (data.tag === 'check') {
      this.title = data.check_id;
      this.points = undefined;
      this.gameTime = undefined;
      this.table = [
        entry('Source', data.src_hostname),
        entry('Destination', data.dst_hostname),
        entry('Check ID', data.check_id),
        entry('Critical', data.critical),
      ];
    }

    // Add score to the first place in table if defined
    if (data.tag === 'automatic-log-with-score' || data.tag === 'manual-log-with-score') {
      this.table = [
        entry('Score', data.score),
        ...this.table,
      ];
    }
  }
}

/**
 * Helper function to simplify creation of table entries
 * @param key Row key
 * @param value Row value
 * @param classes CSS classes to apply to this row
 */
function entry(key: string, value: any, classes?: string[]) {
  return { key, value: value.toString(), classes: classes || [] };
}
