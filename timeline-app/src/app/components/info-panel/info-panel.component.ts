import { Component } from '@angular/core';
import { InfoPanelService } from '../../services/info-panel.service';

/** Detail panel on the right side of the main timeline component */
@Component({
  selector: 'app-info-panel',
  templateUrl: './info-panel.component.html',
  styleUrls: ['./info-panel.component.scss']
})
export class InfoPanelComponent {
  constructor(
    private readonly infoPanelService: InfoPanelService,
  ) { }

  data$ = this.infoPanelService.data$;
}
