export interface Exercise {
  start: string;
  duration: number;
}
