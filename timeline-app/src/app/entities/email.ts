export interface Email {
  messageId: string;
  timestamp: string;
  gameTimestamp: string;
  addressFrom: string;
  addressTo: string;
  inReplyTo: string | null;
  subject: string;
  body: string;
  directory: 'received' | 'sent';
  teamId: number;
}
