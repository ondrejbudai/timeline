export interface Team {
  id: number;
  name: string;
  kypoID: string;
  showInScoring: boolean;
  flag: string;
  color: string;
}
