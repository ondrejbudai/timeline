import { ManualLogEntry } from './manual-log-entry';

export interface Objective {
  id: number;
  name: string;
  abbreviation: string;
  description: string;
  phase: number;
  startingTime: string;
  duration: number;
  notifyStartingTime: number;
  minPoints: number;
  maxPoints: number;
  role: string;
}

export interface ObjectiveWithLog extends Objective {
  log: ManualLogEntry;
}
