export interface AutomaticLogEntry {
  id: number;
  team: number;
  src_hostname: string;
  dst_hostname: string;
  check_id: string;
  state: string;
  timestamp: string;
  gameTimestamp: string;
  points: number;
  critical: boolean;
}

export interface Check {
  src_hostname: string;
  dst_hostname: string;
  check_id: string;
  critical: boolean;
}
