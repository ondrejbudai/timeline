export interface ManualLogEntry {
  id: number;
  team: number;
  objective: {
    id: number;
    name: string;
    role: string;
    phase: number;
    abbreviation?: string;
    description?: string;
    startingTime?: string;
    duration?: number;
    notifyStartingTime?: number;
    minPoints?: number;
    maxPoints?: number;
  };
  timestamp: string;
  gameTimestamp: string;
  points: number;
  state: number;
  comment: string;
  files: number;
  canEdit: boolean;
}
