import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';
import { LoginComponent } from './components/login/login.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { FormsModule } from '@angular/forms';
import { TokenInterceptor } from './token.interceptor';
import { ObjectivesComponent } from './components/objectives/objectives.component';
import { TimelinePageComponent } from './components/timeline-page/timeline-page.component';
import { AutomaticLogComponent } from './components/automatic-log/automatic-log.component';
import { ScoreChartComponent } from './components/score-chart/score-chart.component';
import { GridComponent } from './components/grid/grid.component';
import { TimeAxisComponent } from './components/time-axis/time-axis.component';
import { ManualLogComponent } from './components/manual-log/manual-log.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InfoPanelHookDirective } from './directives/info-panel-hook.directive';
import { InfoPanelComponent } from './components/info-panel/info-panel.component';
import {
  MatButtonModule,
  MatCardModule, MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatMenuModule, MatProgressSpinnerModule, MatSnackBarModule,
  MatToolbarModule
} from '@angular/material';
import { EmailComponent } from './components/email/email.component';
import { PercentOfExerciseDurationPipe } from './pipes/percent-of-exercise-duration.pipe';
import { TooltipComponent } from './components/tooltip/tooltip.component';
import { InfoDetailComponent } from './components/info-detail/info-detail.component';

/** Main application module */
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TimelineComponent,
    ObjectivesComponent,
    TimelinePageComponent,
    AutomaticLogComponent,
    ScoreChartComponent,
    GridComponent,
    TimeAxisComponent,
    ManualLogComponent,
    InfoPanelHookDirective,
    InfoPanelComponent,
    EmailComponent,
    PercentOfExerciseDurationPipe,
    TooltipComponent,
    InfoDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    OAuthModule.forRoot(),
    FormsModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatIconModule,
    MatToolbarModule,
    MatMenuModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatProgressSpinnerModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent],
  entryComponents: [TooltipComponent]
})
export class AppModule {
}
