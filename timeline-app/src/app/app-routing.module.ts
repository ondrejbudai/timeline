import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JwksValidationHandler, OAuthService } from 'angular-oauth2-oidc';
import { authConfig } from './auth.config';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './auth.guard';
import { TimelinePageComponent } from './components/timeline-page/timeline-page.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'timeline', component: TimelinePageComponent, canActivate: [AuthGuard] },
];

/** Module responsible for routing */
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(
    private oAuthService: OAuthService
  ) {
    this.oAuthService.configure(authConfig);
    this.oAuthService.tokenValidationHandler = new JwksValidationHandler();

  }


}
