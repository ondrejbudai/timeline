import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { InfoPanelService } from '../services/info-panel.service';
import { Data, DataTag, TaggedData, TaggedId } from '../util-interfaces/types';
import { Overlay, OverlayPositionBuilder, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { TooltipComponent } from '../components/tooltip/tooltip.component';
import { Highlightable } from '../util-interfaces/highlightable';
import { HighlightService } from '../services/highlight.service';

/** Directive for showing tooltip, detail panel and related entities. */
@Directive({
  selector: '[appInfoPanelHook]'
})
export class InfoPanelHookDirective implements Highlightable {
  private overlayRef: OverlayRef;
  private taggedData: TaggedData;
  private taggedId: TaggedId;

  /**
   * Sets data bound to this directive.
   * @param data Data to be bounded
   */
  @Input('appInfoPanelHook')
  set boundData(data: { data: Data, tag: DataTag, id?: number }) {
    this.taggedData = {
      ...data.data,
      tag: data.tag
    } as TaggedData;

    // Unregister former data from highlight service
    if (this.taggedId) {
      this.highlightService.unregister(this.taggedId, this);
    }

    this.taggedId = undefined;

    // Register new data to highlight service
    if (data.id) {
      this.taggedId = {
        id: data.id,
        tag: data.tag
      };

      this.highlightService.register(this.taggedId, this);

    }
  }

  constructor(
    private readonly infoPanelService: InfoPanelService,
    private readonly overlay: Overlay,
    private readonly overlayPositionBuilder: OverlayPositionBuilder,
    private readonly elementRef: ElementRef,
    private readonly highlightService: HighlightService
  ) {}

  /**
   * Returns element to which is directive applied
   */
  get nativeElementRef() {
    return this.elementRef.nativeElement as HTMLElement;
  }

  /**
   * Shows tooltip and highlights related
   * @param event Corresponding mouse event
   */
  @HostListener('mouseover', ['$event'])
  onMouseOver(event: Event) {
    event.stopPropagation();

    const positionStrategy = this.overlayPositionBuilder
      .flexibleConnectedTo(this.elementRef)
      .withPositions([{
        originX: 'end',
        originY: 'center',
        overlayX: 'start',
        overlayY: 'center',
        offsetX: 10,
      }]);

    this.overlayRef = this.overlay.create({ positionStrategy });

    const tooltipComponentPortal = new ComponentPortal(TooltipComponent);
    const tooltipComponentComponentRef = this.overlayRef.attach(tooltipComponentPortal);
    tooltipComponentComponentRef.instance.data = this.taggedData;

    // Highlight related if id is set
    if (this.taggedId) {
      this.highlightService.highlight(this.taggedId, false);
    }
  }

  /**
   * Removes tooltip and unhighlights related
   * @param event Corresponding mouse event
   */
  @HostListener('mouseout', ['$event'])
  onMouseOut(event: Event) {
    event.stopPropagation();
    if (this.overlayRef) {
      this.overlayRef.dispose();
      this.overlayRef = undefined;
    }

    if (this.taggedId) {
      this.highlightService.dehighlight(this.taggedId, false);
    }
  }


  /**
   * Changes content of detail panel and highlights related
   * @param event Corresponding mouse event
   */
  @HostListener('mousedown', ['$event'])
  onMouseDown(event: Event) {
    event.stopPropagation();
    this.infoPanelService.setNewData(this.taggedData, this.taggedId);

    if (this.taggedId) {
      this.highlightService.highlight(this.taggedId, true);
    }
  }

  /**
   * Highlights corresponding element
   * @param hard FLag determining whether hard or soft highlight should be applied
   */
  highlight(hard: boolean) {
    this.nativeElementRef.classList.add(hard ? 'highlight-hard' : 'highlight-soft');
  }
  /**
   * Unhighlights corresponding element
   * @param hard FLag determining whether hard or soft highlight should be removed
   */
  dehighlight(hard: boolean) {
    this.nativeElementRef.classList.remove(hard ? 'highlight-hard' : 'highlight-soft');
  }

}
