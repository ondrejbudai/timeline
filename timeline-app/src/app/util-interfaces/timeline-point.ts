export interface TimelinePoint<ValueClass> {
  readonly y: number;
  readonly data: ValueClass;
}

export interface TimelineInterval<ValueClass> {
  start: number;
  duration: number;
  data: ValueClass;
}

export function intervalEnd<T>(interval: TimelineInterval<T>) {
  return interval.start + interval.duration;
}
