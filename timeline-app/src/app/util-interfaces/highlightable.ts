export interface Highlightable {
  highlight(hard: boolean);
  dehighlight(hard: boolean);
}
