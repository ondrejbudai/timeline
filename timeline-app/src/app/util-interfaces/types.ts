import { ManualLogEntry } from '../entities/manual-log-entry';
import { AutomaticLogEntry, Check } from '../entities/automatic-log-entry';
import { ObjectiveWithLog } from '../entities/objective';
import { Email } from '../entities/email';

interface Score {
  score: number;
}

export type TaggedManualLogEntry = ManualLogEntry & { tag: 'manual-log' };
export type TaggedManualLogEntryWithScore = ManualLogEntry & Score & { tag: 'manual-log-with-score' };
export type TaggedAutomaticLogEntry = AutomaticLogEntry & { tag: 'automatic-log' };
export type TaggedAutomaticLogEntryWithScore = AutomaticLogEntry & Score & { tag: 'automatic-log-with-score' };
export type TaggedObjectiveWithLog = ObjectiveWithLog & { tag: 'objective' };
export type TaggedEmail = Email & { tag: 'email' };
export type TaggedCheck = Check & { tag: 'check' };

export type TaggedData =
  TaggedManualLogEntry
  | TaggedManualLogEntryWithScore
  | TaggedAutomaticLogEntry
  | TaggedAutomaticLogEntryWithScore
  | TaggedObjectiveWithLog
  | TaggedEmail
  | TaggedCheck;

export type DataTag = TaggedData['tag'];
type Without<T, K> = Pick<T, Exclude<keyof T, K>>;
export type Data = Without<TaggedData, 'tag'>;

export interface TaggedId {
  id: number;
  tag: DataTag;
}
