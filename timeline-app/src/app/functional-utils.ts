/**
 * Groups array to map
 * @param array Array to be grouped
 * @param iteratee Function to generate keys from array
 */
export function groupByToMap<K, T>(array: T[], iteratee: (item: T) => K): Map<K, T[]> {
  const map = new Map<K, T[]>();

  array.forEach((item) => {
    const key = iteratee(item);
    const bucket = map.get(key);

    if (bucket) {
      bucket.push(item);
    } else {
      map.set(key, [item]);
    }
  });

  return map;
}

/**
 * Maps Map to array
 * @param map The map to be mapped
 * @param mapper Mapper function
 */
export function mapMapToArray<Key, Value, Result>(map: Map<Key, Value>, mapper: (value: Value) => Result): Result[] {
  const array = new Array<Result>();
  map.forEach((value) => {
    array.push(mapper(value));
  });

  return array;
}
