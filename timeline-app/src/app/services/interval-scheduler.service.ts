import { Injectable } from '@angular/core';
import { intervalEnd, TimelineInterval } from '../util-interfaces/timeline-point';

/**
 * Checks whether two intervals are compatible
 * @param a First interval
 * @param b Second interval
 * @param offset Minimal time between two intervals
 */
function areIntervalsCompatible<T>(a: TimelineInterval<T>, b: TimelineInterval<T>, offset: number) {
  return a.start - offset >= intervalEnd(b) + offset || b.start - offset >= intervalEnd(a) + offset;
}

/** Class representing group of intervals */
class IntervalGroup<T> {
  /** Array of intervals in this group */
  readonly intervals: TimelineInterval<T>[] = [];

  /**
   * Checks whether interval is compatible with this group
   * @param newInterval Interval checked for compatibility
   * @param offset Minimal time between two intervals
   */
  isCompatibleWith(newInterval: TimelineInterval<T>, offset) {
    return this.intervals.every(interval => areIntervalsCompatible(interval, newInterval, offset));
  }

  /**
   * Adds a new interval into this group
   * @param interval Interval to be added
   */
  add(interval: TimelineInterval<T>) {
    this.intervals.push(interval);
  }
}

/** Service responsible for scheduling intervals to groups containing only non-overlapping ones */
@Injectable({
  providedIn: 'root'
})
export class IntervalSchedulerService {

  /**
   * Schedules intervals into non-overlapping groups
   * @param intervals Intervals to be scheduled
   * @param offset Minimal time between two intervals
   */
  //noinspection JSMethodCanBeStatic
  schedule<T>(intervals: TimelineInterval<T>[], offset: number = 0) {
    // sort by start ascending
    intervals.sort((a, b) => a.start - b.start);

    const intervalGroups: IntervalGroup<T>[] = [];

    intervals.forEach((interval) => {
      // find compatible group and add interval into it, otherwise create a new group
      const compatibleGroup = intervalGroups.find(group => group.isCompatibleWith(interval, offset));

      if (compatibleGroup) {
        compatibleGroup.add(interval);
      } else {
        const newGroup = new IntervalGroup<T>();
        newGroup.add(interval);
        intervalGroups.push(newGroup);
      }
    });

    return intervalGroups.map(group => group.intervals);
  }
}
