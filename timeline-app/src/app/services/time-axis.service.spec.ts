import { TestBed } from '@angular/core/testing';

import { TimeAxisService } from './time-axis.service';

describe('TimeAxisService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TimeAxisService = TestBed.get(TimeAxisService);
    expect(service).toBeTruthy();
  });
});
