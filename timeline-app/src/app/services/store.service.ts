import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Team } from '../entities/team';
import { Email } from '../entities/email';
import { AutomaticLogEntry } from '../entities/automatic-log-entry';
import { Objective } from '../entities/objective';
import { ManualLogEntry } from '../entities/manual-log-entry';

/** Service responsible for storing all data for one team */
@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private readonly pixelsPerHoursStore = new BehaviorSubject<number>(100);
  public readonly pixelsPerHour$ = this.pixelsPerHoursStore.asObservable();

  set pixelsPerHour(pixelsPerHour: number) {
    this.pixelsPerHoursStore.next(pixelsPerHour);
  }

  get pixelsPerHour() {
    return this.pixelsPerHoursStore.value;
  }

  private readonly teamStore = new BehaviorSubject<Team>(undefined);
  public readonly team$ = this.teamStore.asObservable();

  set team(team: Team) {
    this.teamStore.next(team);
  }

  private readonly emailsStore = new BehaviorSubject<Email[]>([]);
  public readonly emails$ = this.emailsStore.asObservable();

  set emails(emails: Email[]) {
    this.emailsStore.next(emails);
  }

  private readonly automaticLogStore = new BehaviorSubject<AutomaticLogEntry[]>([]);
  public readonly automaticLog$ = this.automaticLogStore.asObservable();

  set automaticLog(automaticLog: AutomaticLogEntry[]) {
    this.automaticLogStore.next(automaticLog);
  }

  private readonly manualLogStore = new BehaviorSubject<ManualLogEntry[]>([]);
  public readonly manualLog$ = this.manualLogStore.asObservable();

  set manualLog(manualLog: ManualLogEntry[]) {
    this.manualLogStore.next(manualLog);
  }

  private readonly objectivesStore = new BehaviorSubject<Objective[]>([]);
  public readonly objectives$ = this.objectivesStore.asObservable();

  set objectives(objectives: Objective[]) {
    this.objectivesStore.next(objectives);
  }

  public exercise: { start: Date, duration: number };
}
