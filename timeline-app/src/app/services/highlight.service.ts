import { Injectable } from '@angular/core';
import { Highlightable } from '../util-interfaces/highlightable';
import { TaggedId } from '../util-interfaces/types';

/** Service responsible for highlighting */
@Injectable({
  providedIn: 'root'
})
export class HighlightService {

  /** Map of registered highlightables */
  private readonly registeredHighlightables = new Map<string, Array<Highlightable>>();

  constructor() {}

  /**
   * Registers new Highlightable
   * @param taggedId Highlightable's id
   * @param highlightable Highlightable
   */
  register(taggedId: TaggedId, highlightable: Highlightable) {
    const key = taggedIdToKey(taggedId);
    const bucket = this.registeredHighlightables.get(key);

    // poor man's multimap
    if (bucket) {
      bucket.push(highlightable);
    } else {
      this.registeredHighlightables.set(key, [highlightable]);
    }
  }

  /**
   * Highlights all Highlightables with specified ID
   * @param taggedId ID to be highlighted
   * @param hard Flag indicating the level of highlighting
   */
  highlight(taggedId: TaggedId, hard: boolean) {
    this.getHighlightables(taggedId).forEach((highlightable) => highlightable.highlight(hard));
  }

  /**
   * Dehighlights all Highlightables with specified ID
   * @param taggedId ID to be dehighlighted
   * @param hard Flag indicating the level of highlighting
   */
  dehighlight(taggedId: TaggedId, hard: boolean) {
    this.getHighlightables(taggedId).forEach((highlightable) => highlightable.dehighlight(hard));
  }

  /**
   * Returns all registered highlightables with specified IDs
   * @param taggedId ID to be searched
   */
  private getHighlightables(taggedId: TaggedId): Highlightable[] {
    return this.registeredHighlightables.get(taggedIdToKey(taggedId));
  }

  /**
   * Unregisters specified highlightable
   * @param taggedId Highlightable ID
   * @param highlightable Highlightable
   */
  unregister(taggedId: TaggedId, highlightable: Highlightable) {
    const key = taggedIdToKey(taggedId);
    const bucket = this.registeredHighlightables.get(key);

    if (bucket.length > 1) {
      bucket.splice(bucket.indexOf(highlightable), 1);
    } else {
      this.registeredHighlightables.delete(key);
    }
  }
}

/**
 * Generates key to Highlightable Map
 * @param taggedId Highlightable ID
 */
function taggedIdToKey(taggedId: TaggedId): string {
  let tag = taggedId.tag;
  if (tag === 'manual-log-with-score' || tag === 'manual-log') {
    tag = 'objective';
  }

  if (tag === 'automatic-log-with-score') {
    tag = 'automatic-log';
  }

  return `${tag}:${taggedId.id}`;
}
