import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { from, Observable } from 'rxjs';
import { Email } from '../entities/email';
import { AutomaticLogEntry } from '../entities/automatic-log-entry';
import { getBackendUrl } from '../../globals';
import { Objective } from '../entities/objective';
import { Team } from '../entities/team';
import { Exercise } from '../entities/exercise';
import { ManualLogEntry } from '../entities/manual-log-entry';

/** Service responsible for communicating with backend API */
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private emailsCache?: Email[];
  private objectivesCache?: Objective[];

  constructor(private readonly httpClient: HttpClient) {}

  /** Downloads e-mails for specified team */
  getEmails(team: Team): Observable<Email[]> {
    if (this.emailsCache) {
      return from([this.emailsCache.filter(email => email.teamId === team.id)]);
    }
    return this.httpClient.get<Email[]>(getBackendUrl('/emails'))
      .pipe(map((emails) => {
        this.emailsCache = emails;
        return emails.filter(email => email.teamId === team.id);
      }));
  }

  /** Downloads automatic log for specified team */
  getAutomaticLog(team: Team): Observable<AutomaticLogEntry[]> {
    return this.httpClient.get<AutomaticLogEntry[]>(getBackendUrl(`/logs/automatic?team=${team.id}`));
  }

  /** Downloads manual log for specified team */
  getManualLog(team: Team): Observable<ManualLogEntry[]> {
    return this.httpClient.get<ManualLogEntry[]>(getBackendUrl(`/logs/manual?team=${team.id}`));
  }

  /** Downloads objectives */
  getObjectives(): Observable<Objective[]> {
    if (this.objectivesCache) {
      return from([this.objectivesCache]);
    }
    return this.httpClient.get<Objective[]>(
      getBackendUrl('/objectives'),
      { params: new HttpParams().set('planned', 'true') }
    ).pipe(
      tap((objectives) => { this.objectivesCache = objectives; })
    );
  }

  /** Downloads teams */
  getTeams(): Observable<Team[]> {
    return this.httpClient.get<Team[]>(getBackendUrl('/teams'));
  }

  /** Downloads current exercise */
  getExercise(): Observable<Exercise> {
    return this.httpClient.get<Exercise>(getBackendUrl('/exercises'));
  }
}
