import { Injectable } from '@angular/core';
import { StoreService } from './store.service';
import { TimeConverterService } from './time-converter.service';
import { map } from 'rxjs/operators';
import { addHours } from 'date-fns';

/** Service responsible for generating time axis and grid */
@Injectable({
  providedIn: 'root'
})
export class TimeAxisService {

  constructor(
    private readonly store: StoreService,
    private readonly timeConverter: TimeConverterService
  ) {}

  /** Points for time axis */
  readonly timeAxisPoints$ = this.store.pixelsPerHour$.pipe(map((pixelsPerHour) => {
    const hours = this.store.exercise.duration;

    const numberOfLinesInHour = Math.floor(pixelsPerHour / 50);

    const numberOfLines = hours * numberOfLinesInHour;

    return new Array(Math.floor(numberOfLines)).fill(0).map((value, index) => {
      const time = addHours(this.store.exercise.start, index / numberOfLinesInHour).toISOString();

      const data = {
        timestamp: time,
        time: this.timeConverter.toTimeGameFromISODateTime(time, false),
      };

      return this.timeConverter.toTimelinePointFromISODateTime(data, data.timestamp);
    });
  }));
}
