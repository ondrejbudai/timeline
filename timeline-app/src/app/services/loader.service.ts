import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { StoreService } from './store.service';
import { Observable, zip } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Team } from '../entities/team';

/** Service responsible for loading the data from backend */
@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  constructor(
    private apiService: ApiService,
    private store: StoreService
  ) { }

  /**
   * Loads data for specified team
   * @param team Team
   */
  load(team: Team): Observable<void> {
    return zip(
      this.apiService.getEmails(team).pipe(tap((emails) => { this.store.emails = emails; })),
      this.apiService.getManualLog(team).pipe(tap((manualLog) => { this.store.manualLog = manualLog; })),
      this.apiService.getAutomaticLog(team).pipe(tap((automaticLog) => { this.store.automaticLog = automaticLog; })),
      this.apiService.getObjectives().pipe(tap((objectives) => { this.store.objectives = objectives; })),
    ).pipe(
      map(() => {})
    );
  }

  /**
   * Loads exercise data
   */
  loadExercise(): Observable<void> {
    return this.apiService.getExercise()
      .pipe(map(({ duration, start }) => {
        this.store.exercise = {
          duration: duration + 0.5,
          start: new Date(start)
        };
      }));
  }
}
