import { Injectable } from '@angular/core';
import { AutomaticLogEntry } from '../entities/automatic-log-entry';
import { map } from 'rxjs/operators';
import { groupBy, minBy, orderBy } from 'lodash-es';
import { groupByToMap, mapMapToArray } from '../functional-utils';
import { CRITICAL_CHECK_DURATION_IN_MINUTES, NON_CRITICAL_CHECK_DURATION_IN_MINUTES } from '../../globals';
import { StoreService } from './store.service';
import { TimeConverterService } from './time-converter.service';

/** Service responsible for preproccessing automatic log */
@Injectable({
  providedIn: 'root'
})
export class AutomaticLogService {
  constructor(
    private readonly store: StoreService,
    private readonly timeConverter: TimeConverterService
  ) { }

  /** Automatic log entries grouped by source, destination and checkId */
  readonly automaticLogCols$ = this.store.automaticLog$.pipe(
    map((log) => {
      // filter out entries with no score change and rename citizen deaths
      const filteredLog = log
        .filter(logEntry => logEntry.points !== 0)
        .map((logEntry) => {
          if (logEntry.check_id === 'citizen-death') {
            logEntry.check_id = 'death';
          }

          return logEntry;
        });

      // group by check, src and dest
      const groupedLog = groupByToMap(filteredLog, (logEntry) => {
        return `${logEntry.check_id}${logEntry.src_hostname}${logEntry.dst_hostname}`;
      });

      // convert to TimelinePoints
      return mapMapToArray(groupedLog, (entries) => {
        const intervals = this.mapOneCheckLogEntriesToTimelinePoints(entries);

        return {
          check: {
            check_id: entries[0].check_id,
            src_hostname: entries[0].src_hostname,
            dst_hostname: entries[0].dst_hostname,
            critical: entries[0].critical,
          },
          intervals
        };
      });
    })
  );

  /** Logs grouped by check type, then by destination, then by (checkId, destination, source) */
  readonly logGroups$ = this.automaticLogCols$.pipe(map((automaticLog) => {
    // Criticals go first
    const sortedLog = orderBy(automaticLog, ['critical'], ['desc']);

    // Group by check type
    const columnGroups = groupBy(sortedLog, logColumn => logColumn.check.check_id.split('/')[0]);
    return Object.entries(columnGroups)
      .map(([name, columns]) => {

        // group by destination
        const columnsByDest = Object.entries(groupBy(columns, column => column.check.dst_hostname))
          .map(([destination, columnsByDestination]) => ({
            destination,
            columns: columnsByDestination,
          }));

        return {
          name,
          columns: columnsByDest,
          totalColumns: columnsByDest.reduce((sum, col) => sum + col.columns.length, 0),
        };
      });
  }));


  /**
   * Maps log entries to TimelinePoints
   * @param oneCheckLogEntries Log entries to be mapped
   */
  private mapOneCheckLogEntriesToTimelinePoints(oneCheckLogEntries: AutomaticLogEntry[]) {
    const minPoints = minBy(oneCheckLogEntries.filter(entry => entry.points !== 0), logEntry => -logEntry.points).points;

    return oneCheckLogEntries.map((logEntry) => this.mapLogEntryToTimelinePoint(logEntry, logEntry.points / minPoints));
  }

  /**
   * Maps one log entry to TimelinePoint
   * @param logEntry Log entry to be mapped
   * @param durationMultiplier Factor of log entry duration
   */
  private mapLogEntryToTimelinePoint(logEntry: AutomaticLogEntry, durationMultiplier: number) {
    const baseDuration = logEntry.critical ? CRITICAL_CHECK_DURATION_IN_MINUTES : NON_CRITICAL_CHECK_DURATION_IN_MINUTES;
    const duration = baseDuration * durationMultiplier;

    return this.timeConverter.toTimelineIntervalFromISODateTime(logEntry, logEntry.timestamp, duration);
  }
}




