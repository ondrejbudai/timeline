import { Injectable } from '@angular/core';
import { StoreService } from './store.service';
import { map } from 'rxjs/operators';
import { groupBy } from 'lodash-es';
import { combineLatest } from 'rxjs';

/** Service responsible for preproccessing manual log */
@Injectable({
  providedIn: 'root'
})
export class ManualLogService {
  constructor(
    private readonly store: StoreService
  ) {}

  /**
   * Manual log with full objectives (API doesn't return full objection for manual log)
   */
  readonly manualLogWithFullObjectives$ =
    combineLatest(this.store.manualLog$, this.store.objectives$).pipe(map(([rawManualLog, objectives]) => {
      return rawManualLog.map((rawManualLogEntry) => {
        const foundObjective = objectives.find(objective => objective.id === rawManualLogEntry.objective.id);

        if (!foundObjective) {
          return rawManualLogEntry;
        }

        return {
          ...rawManualLogEntry,
          objective: foundObjective
        };
      });
    }));

  /**
   * Manual log grouped by roles.
   */
  readonly manualLogGroupedByRole$ = this.manualLogWithFullObjectives$.pipe(map((manualLog) => {
    const logByRoles = groupBy(manualLog, logEntry => logEntry.objective.role);

    return Object.entries(logByRoles).map(([role, entries]) => {
      if (role === 'blondie') {
        role = 'blond';
      }
      return { role, entries };
    });
  }));
}
