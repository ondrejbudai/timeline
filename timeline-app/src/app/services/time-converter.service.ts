import { Injectable } from '@angular/core';
import { StoreService } from './store.service';
import { TimelineInterval, TimelinePoint } from '../util-interfaces/timeline-point';
import { differenceInSeconds } from 'date-fns';

/** Service responsible for converting time */
@Injectable({
  providedIn: 'root'
})
export class TimeConverterService {

  constructor(private readonly store: StoreService) { }

  /**
   * Converts data and ISO date to TimelinePoint
   * @param data Data
   * @param isoDateTime ISO date
   */
  toTimelinePointFromISODateTime<T>(data: T, isoDateTime: string): TimelinePoint<T> {
    const secondsFromStart = differenceInSeconds(new Date(isoDateTime), this.store.exercise.start);
    return {
      y: secondsFromStart / 3600,
      data
    };
  }

  /**
   * Converts data, time from beginning of exercise and duration to TimelineInterval
   * @param data Data
   * @param timeFromBeginning Time from beginning of exercise
   * @param durationInMinutes Duration
   */
  toTimelineIntervalFromTimeFromBeginning<T>(data: T, timeFromBeginning: string, durationInMinutes: number): TimelineInterval<T> {
    const [hours, minutes] = timeFromBeginning.split(':').map(str => +str);
    return {
      start: hours + minutes / 60,
      duration: durationInMinutes / 60,
      data,
    };
  }

  /**
   * Convert data, ISO date and duration to TimelineInterval
   * @param data Data
   * @param isoDateTime ISO date
   * @param durationInMinutes Duration
   */
  toTimelineIntervalFromISODateTime<T>(data: T, isoDateTime: string, durationInMinutes: number): TimelineInterval<T> {
    const secondsFromStart = differenceInSeconds(new Date(isoDateTime), this.store.exercise.start);
    return {
      start: secondsFromStart / 3600,
      duration: durationInMinutes / 60,
      data,
    };
  }

  /**
   * Convert ISO date to time from beginning of exercise
   * @param isoDateTime ISO date
   * @param includeSeconds Flag indicating whether output should contain seconds
   */
  toTimeGameFromISODateTime(isoDateTime: string, includeSeconds: boolean = true): string {
    const secondsFromStart = differenceInSeconds(new Date(isoDateTime), this.store.exercise.start);
    return formatTimeGame(secondsFromStart, includeSeconds);
  }
}

/**
 * Formats seconds to H:M:S or H:M
 * @param secondsFromStart Seconds
 * @param includeSeconds Flag indicating whether seconds shall be included
 */
function formatTimeGame(secondsFromStart: number, includeSeconds: boolean): string {
  const hours = Math.floor(secondsFromStart / 3600);
  const minutes = Math.floor((secondsFromStart % 3600) / 60);
  const seconds = secondsFromStart % 60;
  if (includeSeconds) {
    return `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
  }

  return `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}`;
}
