import { TestBed } from '@angular/core/testing';

import { AutomaticLogService } from './automatic-log.service';

describe('AutomaticLogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AutomaticLogService = TestBed.get(AutomaticLogService);
    expect(service).toBeTruthy();
  });
});
