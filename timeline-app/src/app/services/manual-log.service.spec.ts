import { TestBed } from '@angular/core/testing';

import { ManualLogService } from './manual-log.service';

describe('ManualLogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManualLogService = TestBed.get(ManualLogService);
    expect(service).toBeTruthy();
  });
});
