import { Injectable } from '@angular/core';
import { combineLatest } from 'rxjs';
import { ObjectiveWithLog } from '../entities/objective';
import { StoreService } from './store.service';
import { map } from 'rxjs/operators';
import { IntervalSchedulerService } from './interval-scheduler.service';
import { TimeConverterService } from './time-converter.service';

/** Service responsible for preproccessing objectives */
@Injectable({
  providedIn: 'root'
})
export class ObjectiveService {

  constructor(
    private store: StoreService,
    private intervalSchedulerService: IntervalSchedulerService,
    private timeConverterService: TimeConverterService,
  ) { }

  /** Objectives with filled in manual log entries */
  readonly objectivesWithLogEntries$ = combineLatest(this.store.objectives$, this.store.manualLog$)
    .pipe(map(([objectives, manualLogs]) => {
      return objectives.map((objective) => {
        const objectiveWithLog = objective as ObjectiveWithLog;
        objectiveWithLog.log = manualLogs.find(log => log.objective.id === objectiveWithLog.id);
        return objectiveWithLog;
      });
    }));

  /** Objectives grouped to non-overlapping groups */
  readonly groupedObjectives$ = this.objectivesWithLogEntries$.pipe(map((objectives) => {
    const intervals = objectives.map((objective) => {
      return this.timeConverterService.toTimelineIntervalFromTimeFromBeginning(objective, objective.startingTime, objective.duration);
    });

    return this.intervalSchedulerService.schedule(intervals);
  }));
}
