import { TestBed } from '@angular/core/testing';

import { IntervalSchedulerService } from './interval-scheduler.service';

describe('IntervalSchedulerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IntervalSchedulerService = TestBed.get(IntervalSchedulerService);
    expect(service).toBeTruthy();
  });
});
