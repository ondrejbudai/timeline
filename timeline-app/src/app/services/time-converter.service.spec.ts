import { TestBed } from '@angular/core/testing';

import { TimeConverterService } from './time-converter.service';

describe('TimeConverterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TimeConverterService = TestBed.get(TimeConverterService);
    expect(service).toBeTruthy();
  });
});
