import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { TaggedData, TaggedId } from '../util-interfaces/types';
import { HighlightService } from './highlight.service';

/** Service responsible for showing details in side panel. */
@Injectable({
  providedIn: 'root'
})
export class InfoPanelService {
  private readonly dataStore = new Subject<TaggedData>();
  readonly data$ = this.dataStore.asObservable();
  private currentId?: TaggedId;


  constructor(
    private readonly highlightService: HighlightService
  ) {}

  /**
   * Sets newly showed detail
   * @param data Data of the detail
   * @param id Tag and ID of data
   */
  setNewData(data: TaggedData, id?: TaggedId) {
    this.dataStore.next(data);

    if (this.currentId) {
      this.highlightService.dehighlight(this.currentId, true);
    }

    this.currentId = undefined;

    if (id) {
      this.currentId = id;
    }
  }
}
