import { Pipe, PipeTransform } from '@angular/core';
import { StoreService } from '../services/store.service';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

/** Converts time in hours to percentage of exercise duration */
@Pipe({
  name: 'percentOfExerciseDuration'
})
export class PercentOfExerciseDurationPipe implements PipeTransform {

  constructor(
    private readonly store: StoreService,
    private readonly sanitizer: DomSanitizer
  ) {}

  /**
   * Transform method
   * @param timeInHours Time of hours
   * @param offset Offset of position used in CSS calc expression
   */
  transform(timeInHours: number, offset?: string): SafeStyle {
    const percentValue = timeInHours / this.store.exercise.duration * 100 + '%';
    if (offset !== undefined) {
      return this.sanitizer.bypassSecurityTrustStyle(`calc(${percentValue} + ${offset})`);
    }
    return this.sanitizer.bypassSecurityTrustStyle(percentValue);
  }

}
