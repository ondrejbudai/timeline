import {AuthConfig} from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  // URL to oauth2 token endpoint
  tokenEndpoint: '/cyberex/api-auth/v1/token/',

  // URL of the SPA to redirect the user to after login
  redirectUri: window.location.origin,


  clientId: 'tcZgm51c8GMVRLCF2H4AarnENVk7MdXlfxJlg7pl',

  // tslint:disable-next-line:max-line-length
  dummyClientSecret: '50tSrIZ3Z0RdR8ZumqMX9IJgkgXnu7xzQB9BL17yErta0CVChbcR0ybrax3g4O7ChSpt7NEVhPDJnlLobGucLMYZmS5G2qJNcLWgfskDNqyLFQF7chcJsgR80O7Xr0Fa',

  // set the scope for the permissions the client should request
  scope: 'read',

  requireHttps: false
};
