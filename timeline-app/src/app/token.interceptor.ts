import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

/** Interceptor responsible for adding OUath Header to all HTTP requests */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private oAuthService: OAuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({
      setHeaders: {
        Authorization: this.oAuthService.authorizationHeader()
      }
    });

    return next.handle(req);
  }


}
