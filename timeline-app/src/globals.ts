// URL to backed
export const BACKEND_URL = 'cyberex/api/v1';

export const getBackendUrl = (url: string): string => `${BACKEND_URL}${url}`;

export const CRITICAL_CHECK_DURATION_IN_MINUTES = 3; // duration in minutes
export const NON_CRITICAL_CHECK_DURATION_IN_MINUTES = 6; // duration in minutes

// starting score
export const START_SCORE = 100000;
