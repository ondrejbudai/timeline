# Timeline diploma thesis

Author: Ondřej Budai <budai@mail.muni.cz>

![screenshot of app](screenshot.png)

## Prerequisites
- Node.js 10
- NPM
- Vagrant (for backend)

## How to run
There're currently two options how to run this app, with mock data or with real data:

### Mock data
Clone backend and start it:
```
git clone git@gitlab.ics.muni.cz:kypo2/cyberex-scoring/cyberex-control-app.git
vagrant up
```

Now switch back to this repository and modify `timeline-app/src/app/auth.config.ts` to contain client ID and client secret specified in README of backend application

Install dependencies and run the application:

```
cd timeline-app
npm install
npm start
```

### Real data
Clone backend and don't start it yet.
```
git clone git@gitlab.ics.muni.cz:kypo2/cyberex-scoring/cyberex-control-app.git
```

Open the cloned backend and download dataset from gitlab wiki of timeline project and extract it into `install` directory as `database.json`. Open `install/prepare-application.sh` and find 3 "pipenv run python manage.py" commands and replace them with following command: "pipenv run python manage.py loaddata database".

Start the backend:
`vagrant up`

Now switch back to this repository and install dependencies and run the application:
```
cd timeline-app
npm install
npm start
```

## How to use data-converter
Consult README in data-converter directory
